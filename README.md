﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/77c80d023cdbb58b03fa8367b709d52f.png) 

Doom II setup/launch script powered by GZDoom, this is also a set of AUR packages.

The binaries were compiled from the official GZDoom repo
https://github.com/ZDoom/gzdoom

This has no affiliation with ID software and uses original wad files from an archive to preserve the game being kept alive by open source engines

If you want to support ID software please buy the game  from Steam or GOG

[Steam](https://store.steampowered.com/app/2300/DOOM_II/))

[GOG](https://www.gog.com/en/game/doom_ii))

Packages: 

[Doom II](https://aur.archlinux.org/packages/doom2))

[gzdoom-bin](https://aur.archlinux.org/packages/gzdoom-bin))

 ### Author
  * Corey Bruce
